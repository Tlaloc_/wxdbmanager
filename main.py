# -*- coding: utf-8 -*- 

import wx
import wx.xrc
import wx.aui
import sys

from Controller import Controller

class main ( wx.Frame ):

	List_tables = []
	List_databases = []

	database_active = ''
	database_old_selected = ''

	table_active = ''
	table_old_selection = ''

	object_item = None
	sub_object_item = None

	controller = Controller()

	is_connected = False

	option_menu = []
	
	def __init__( self, parent ):

		self.check_connection()

		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		self.m_menubar1 = wx.MenuBar( 0 )
		self.m_menu1 = wx.Menu()
		self.m_menubar1.Append( self.m_menu1, u"MyMenu" ) 
		
		self.SetMenuBar( self.m_menubar1 )
		
		self.m_toolBar1 = self.CreateToolBar( wx.TB_HORIZONTAL, wx.ID_ANY ) 
		self.m_tool1 = self.m_toolBar1.AddLabelTool( wx.ID_ANY, u"tool", wx.Bitmap( u"icon/db.png", wx.BITMAP_TYPE_ANY ), wx.NullBitmap, wx.ITEM_NORMAL, wx.EmptyString, wx.EmptyString, None ) 
		
		self.m_toolBar1.Realize() 
		
		bSizer1 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.tree = wx.TreeCtrl( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TR_DEFAULT_STYLE )
		bSizer1.Add( self.tree, 1, wx.ALL|wx.EXPAND, 5 )

		self.add_items_tree()
		
		self.notebook = wx.aui.AuiNotebook( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.aui.AUI_NB_DEFAULT_STYLE )
		self.m_panel1 = wx.Panel( self.notebook, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL )
		self.notebook.AddPage( about_panel(self.notebook), u"WxDBManager")
		
		bSizer1.Add( self.notebook, 5, wx.EXPAND |wx.ALL, 5  )
		
		
		self.SetSizer( bSizer1 )
		self.Layout()
		
		self.Centre( wx.BOTH )
		self.SetSize((800,600))
		self.SetTitle("WxDBManager")
		self.Show()


		self.Bind(wx.EVT_TOOL,self.conection_manager,self.m_tool1)
	
	def check_connection(self):
		e = self.controller.check_connection_fail()
		if str(e) == 'None':
			self.is_connected = True
		elif e != '':
			wx.MessageBox(str(e),'Alert')
			self.is_connected = False

	def conection_manager(self,e=None):
		NewBD = connection_manager_dialog(self)
		NewBD.ShowModal()
		NewBD.Destroy()

	def get_databases_list(self,tree,root):
		self.items_database_list = {}
		if self.is_connected == True:
			rows = self.controller.get_databases()
			for row in rows:
				item = tree.AppendItem(root, row[0])
				self.items_database_list[row[0]]=(item)
				self.Bind(wx.EVT_TREE_SEL_CHANGED, self.OnSelChanged, tree)

	def add_items_tree(self):
		root = self.tree.AddRoot('Databases')
		self.tree.Expand(root)
		self.get_databases_list(self.tree,root)

	def OnSelChanged(self,event):
		items =  event.GetItem()
		db_nodo = self.tree.GetItemText(items)

		if db_nodo != 'Databases':
			if self.check_is_database(items) == 1:
				self.database_active = db_nodo
				#print(db_nodo)
				if self.database_old_selected == '':
					self.database_old_selected = db_nodo
					self.object_item = self.items_database_list.get(db_nodo)
				else:
					self.database_old_selected = db_nodo
					self.tree.DeleteChildren(self.object_item)
					self.object_item = self.items_database_list.get(db_nodo)

				if self.database_active != '':
					self.add_tables_nodo(self.items_database_list.get(db_nodo), self.tree.GetItemText(items))
					#print(self.List_tables)
				self.option_menu = ['New Table', 'Drop DB']
			else:
				self.table_active = db_nodo
				self.option_menu = ['Select','Edit','Truncate','Add','Drop Table','Describe']
		else:
			self.option_menu = ['New DB']

		self.popupmenu = wx.Menu()
		for text in self.option_menu:
			item = self.popupmenu.Append(-1, text)
			self.Bind(wx.EVT_MENU, self.OnPopupItemSelected, item)
		self.tree.Bind(wx.EVT_CONTEXT_MENU, self.OnShowPopup)

	def check_is_database(self,db):
		valores = self.items_database_list.values()
		n = valores.count(db)
		return n
	
	def add_tables_nodo(self,root,db_name):
		n = len(self.List_tables)
		if n == 0:
			pass
		else:
			self.List_tables = []

		rows = self.controller.get_tables_from_database(self.database_active)
		for row in rows:
			self.tree.AppendItem(root,str(row[0]))
			self.List_tables.append(row[0])
	
	def OnShowPopup(self, event):
		pos = event.GetPosition()
		pos = self.ScreenToClient(pos)
		self.PopupMenu(self.popupmenu, pos)

	def OnPopupItemSelected(self, event):
		item = self.popupmenu.FindItemById(event.GetId())
		text = item.GetText()

		if text == 'Select':
			self.notebook.AddPage( select_table_panel(self.notebook, self.database_active, self.table_active), u"Select Table")
		elif text == 'New DB':
			NewBD = new_database_dialog(self)
			NewBD.ShowModal()
			NewBD.Destroy()
			self.tree.DeleteAllItems()
			self.add_items_tree()

		elif text == 'Drop DB':
			dlg = wx.MessageDialog(None, "Do you want to delete this database?",'Delete',wx.YES_NO | wx.ICON_QUESTION)
			result = dlg.ShowModal()
			if result == wx.ID_YES:
				self.controller.Database_Name=''
				e = self.controller.delete_database(self.database_active)
				print(e)
			else:
				print("No pressed")
			
			self.tree.DeleteAllItems()
			self.add_items_tree()

		elif text == 'New Table':
			NewBD = new_table_dialog(self, self.database_active)
			NewBD.ShowModal()
			NewBD.Destroy()

			self.tree.DeleteChildren(self.items_database_list.get(self.database_active))
			self.add_tables_nodo(self.items_database_list.get(self.database_active), self.database_active)

		elif text == 'Drop Table':
			dlg = wx.MessageDialog(None, "Do you want to delete this table?",'Delete',wx.YES_NO | wx.ICON_QUESTION)
			result = dlg.ShowModal()
			if result == wx.ID_YES:
				self.controller.Database_Name=self.database_active
				self.controller.delete_table(self.table_active,)
				print(self.table_active)
			else:
				print("No pressed")

			self.tree.DeleteChildren(self.items_database_list.get(self.database_active))
			self.add_tables_nodo(self.items_database_list.get(self.database_active), self.database_active)

		elif text == 'Describe':
			self.notebook.AddPage( describe_table_panel(self.notebook, self.database_active, self.table_active), u"Describe Table")

class about_panel(wx.Panel):
	
	def __init__(self, parent):
		wx.Panel.__init__ ( self, parent=parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
		
		bSizer2 = wx.BoxSizer( wx.VERTICAL )
		
		self.m_bitmap1 = wx.StaticBitmap( self, wx.ID_ANY, wx.Bitmap( u"icon/logo_db_64x64.png", wx.BITMAP_TYPE_ANY ), wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer2.Add( self.m_bitmap1, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		self.m_staticText4 = wx.StaticText( self, wx.ID_ANY, u"WxDBManager", wx.DefaultPosition, wx.Size( -1,25 ), 0 )
		self.m_staticText4.Wrap( -1 )
		self.m_staticText4.SetFont( wx.Font( 16, wx.FONTFAMILY_DEFAULT, wx.FONTSTYLE_NORMAL, wx.FONTWEIGHT_BOLD, False, wx.EmptyString ) )
		
		bSizer2.Add( self.m_staticText4, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		self.m_staticText5 = wx.StaticText( self, wx.ID_ANY, u"0.2", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText5.Wrap( -1 )
		bSizer2.Add( self.m_staticText5, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		self.m_staticText6 = wx.StaticText( self, wx.ID_ANY, u"MySQL FrontEnd  made in wxpython", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.m_staticText6.Wrap( -1 )
		bSizer2.Add( self.m_staticText6, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		self.m_hyperlink1 = wx.HyperlinkCtrl( self, wx.ID_ANY, u"Visit us in GitLab Project", u"http://www.wxformbuilder.org", wx.DefaultPosition, wx.DefaultSize, wx.HL_DEFAULT_STYLE )
		bSizer2.Add( self.m_hyperlink1, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		
		self.SetSizer( bSizer2 )
		self.Layout()

class describe_table_panel(wx.Panel ):
	controller = Controller()
	def __init__(self, parent, db, table):
		wx.Panel.__init__ ( self, parent=parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )
		
		self.table = table
		self.db = db

		bSizer2 = wx.BoxSizer( wx.VERTICAL )

		id=wx.NewId()
		self.lista=wx.ListCtrl(self,id,style=wx.LC_REPORT|wx.SUNKEN_BORDER,pos=(0,0),size=(555,280))
		bSizer2.Add(self.lista, 1, wx.EXPAND)

		self.lista.InsertColumn(0, 'Nombre')
		self.lista.InsertColumn(1, 'Field')
		self.lista.InsertColumn(2, 'Type')
		self.lista.InsertColumn(3, 'Null')
		self.lista.InsertColumn(4, 'Key')
		self.lista.InsertColumn(5, 'Default')
		self.lista.InsertColumn(6, 'Extra')

		rows = self.controller.get_header_table("SHOW COLUMNS FROM " + self.table, self.db)
		
		index = 0

		for row in rows:
			index = self.lista.InsertStringItem(sys.maxint, str(row[0]))
			for er in range(1,6):
				self.lista.SetStringItem(index, er, str(row[er]))

		self.SetSizer( bSizer2 )
		self.Layout()

class select_table_panel(wx.Panel):
	"""docstring for select_table_panel"""

	controller = Controller()

	def __init__(self, parent, db, table):
		wx.Panel.__init__ ( self, parent=parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 500,300 ), style = wx.TAB_TRAVERSAL )

		self.table = table
		self.db = db
		bSizer12 = wx.BoxSizer( wx.VERTICAL )

		self.tool2 = wx.ToolBar( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TB_HORIZONTAL ) 
		self.toolDelete = self.tool2.AddLabelTool( wx.ID_ANY, u"tool", wx.Bitmap( u"icon/017-error.png", wx.BITMAP_TYPE_ANY ), wx.NullBitmap, wx.ITEM_NORMAL, u"Delete row", wx.EmptyString, None ) 
		
		self.tool_clear = self.tool2.AddLabelTool( wx.ID_ANY, u"tool", wx.Bitmap( u"icon/cart.png", wx.BITMAP_TYPE_ANY ), wx.NullBitmap, wx.ITEM_NORMAL, u"Clear", wx.EmptyString, None ) 
		
		self.tool_ok = self.tool2.AddLabelTool( wx.ID_ANY, u"tool", wx.Bitmap( u"icon/014-success.png", wx.BITMAP_TYPE_ANY ), wx.NullBitmap, wx.ITEM_NORMAL, u"Save", wx.EmptyString, None ) 
		
		self.tool_add_row = self.tool2.AddLabelTool( wx.ID_ANY, u"tool", wx.Bitmap( u"icon/021-plus.png", wx.BITMAP_TYPE_ANY ), wx.NullBitmap, wx.ITEM_NORMAL, u"Add", wx.EmptyString, None ) 
		
		self.tool2.Realize()
		
		bSizer12.Add( self.tool2, 0, wx.EXPAND, 5 )

		id=wx.NewId()
		self.lista=wx.ListCtrl(self,id,style=wx.LC_REPORT|wx.SUNKEN_BORDER)
		bSizer12.Add(self.lista, 1, wx.EXPAND)
		self.SetSizer( bSizer12 )
		self.Layout()

		self.list_data()

		self.Bind(wx.EVT_TOOL, self.delete_item, self.toolDelete)
		self.Bind(wx.EVT_TOOL, self.truncate_item, self.tool_clear)
		self.Bind(wx.EVT_TOOL, self.add_row, self.tool_add_row)

	def list_data(self):
		rows = self.controller.get_header_table("SHOW COLUMNS FROM " + self.table, self.db)

		c = 0
		for row in rows:
			if c == 0:
				self.primera_columna_db = str(row[0])
			self.lista.InsertColumn(c, row[0])
			c = c+1

		rows = ''
		rows = self.controller.get_data(self.table,self.db)
		arreglo = []
		for row in rows:
			arreglo.append(row)

		for ar in arreglo:
			index = self.lista.InsertStringItem(sys.maxint, str(ar[0]))
			for er in range(1,c):
				self.lista.SetStringItem(index, er, str(ar[er]))

	def delete_item(self, event):
		print('-------------> delete_item ')
		if  self.lista.GetFirstSelected() == -1:
			wx.MessageBox("No data selected",'Info')
		else:
			a = self.lista.GetFirstSelected()
			r = self.lista.GetColumnCount()

			data = []

			for x in xrange(0,r):
				data.append(self.lista.GetItemText(a,x))

			self.controller.delete_table_item(self.table, self.db, data)
			self.lista.ClearAll()
			self.list_data()

	def truncate_item(self, event):
		dlg = wx.MessageDialog(None, "Do you want to truncate this table?",'Truncate',wx.YES_NO | wx.ICON_QUESTION)
		result = dlg.ShowModal()
		if result == wx.ID_YES:
			self.controller.truncate_table(self.db, self.table)
			self.lista.ClearAll()
			self.list_data()
		else:
			print("No pressed")

	def add_row(self, event):
		NewBD = new_row_dialog(self, self.table, self.db)
		NewBD.ShowModal()
		NewBD.Destroy()
	
class connection_manager_dialog ( wx.Dialog ):
	
	controller = Controller()

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Connection", pos = wx.DefaultPosition, size = wx.Size( 258,229 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		gSizer1 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.lbl1 = wx.StaticText( self, wx.ID_ANY, u"Hostname", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl1.Wrap( -1 )
		gSizer1.Add( self.lbl1, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.txtHost = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.txtHost, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.lbl2 = wx.StaticText( self, wx.ID_ANY, u"UserName", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl2.Wrap( -1 )
		gSizer1.Add( self.lbl2, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.txtUser = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( self.txtUser, 1, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.lbl3 = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl3.Wrap( -1 )
		gSizer1.Add( self.lbl3, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.tctPass = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_PASSWORD )
		gSizer1.Add( self.tctPass, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		btnAccept = wx.Button( self, wx.ID_ANY, u"Accept", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( btnAccept, 0, wx.ALL, 5 )
		
		btnTest = wx.Button( self, wx.ID_ANY, u"Test", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer1.Add( btnTest, 0, wx.ALL, 5 )
		
		
		self.SetSizer( gSizer1 )
		self.Layout()
		
		self.Centre( wx.BOTH )

		self.Bind(wx.EVT_BUTTON,self.saveConnection,btnAccept)
		self.Bind(wx.EVT_BUTTON,self.testConnection,btnTest)

	def saveConnection(self, e):
		host = self.txtHost.GetValue()
		user =self.txtUser .GetValue()
		passw =self.tctPass.GetValue()
		#self.controller.read_json()
		#print(host)

		self.controller.Host_Name = self.txtHost.GetValue()
		self.controller.User_Name = self.txtUser .GetValue()
		self.controller.Password = self.tctPass.GetValue()

		self.controller.write_json_conn()

		wx.MessageBox("success operation",'Info')

	def testConnection(self, e):
		e = self.controller.read_json()
		print(e)

class new_database_dialog ( wx.Dialog ):
	controller = Controller()

	def __init__( self, parent ):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"New Database", pos = wx.DefaultPosition, size = wx.Size( 261,142 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer4 = wx.BoxSizer( wx.VERTICAL )
		
		bSizer5 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.lbl1 = wx.StaticText( self, wx.ID_ANY, u"Name", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl1.Wrap( -1 )
		bSizer5.Add( self.lbl1, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		self.txtDatabase = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		bSizer5.Add( self.txtDatabase, 0, wx.ALIGN_CENTER|wx.ALL, 5 )
		
		bSizer4.Add( bSizer5, 0, wx.EXPAND, 5 )
		
		self.btnCreate = wx.Button( self, wx.ID_ANY, u"Create", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer4.Add( self.btnCreate, 0, wx.ALL, 5 )
		
		self.SetSizer( bSizer4 )
		self.Layout()
		
		self.Centre( wx.BOTH )

		self.btnCreate.Bind(wx.EVT_BUTTON,self.create_database)


	def create_database(self, evt):
		db = self.txtDatabase.GetValue()
		n = self.controller.create_new_database(db)
		if n == '':
			print("s")
		else:
			print(n)

class new_table_dialog ( wx.Dialog ):

	table_name = ''
	controller = Controller()
	
	def __init__( self, parent , db):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"New table", pos = wx.DefaultPosition, size = wx.Size( 600,254 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		bSizer3 = wx.BoxSizer( wx.HORIZONTAL )
		
		bSizer7 = wx.BoxSizer( wx.VERTICAL )
		
		gSizer2 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.lbl1 = wx.StaticText( self, wx.ID_ANY, u"Field name:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl1.Wrap( -1 )
		gSizer2.Add( self.lbl1, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.txtName = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.txtName, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.lbl2 = wx.StaticText( self, wx.ID_ANY, u"long", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl2.Wrap( -1 )
		gSizer2.Add( self.lbl2, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.spinLong = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 1, 255, 1 )
		gSizer2.Add( self.spinLong, 0, wx.ALL, 5 )
		
		self.lbl3 = wx.StaticText( self, wx.ID_ANY, u"type", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl3.Wrap( -1 )
		gSizer2.Add( self.lbl3, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		cboTypeChoices = [ u"varchar", u"integer", u"boolean", u"date" ]
		self.cboType = wx.ComboBox( self, wx.ID_ANY, u"varchar", wx.DefaultPosition, wx.DefaultSize, cboTypeChoices, 0 )
		gSizer2.Add( self.cboType, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )

		self.lbl4 = wx.StaticText( self, wx.ID_ANY, u"Default:", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl4.Wrap( -1 )
		gSizer2.Add( self.lbl4, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.txtDefault = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.txtDefault, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.cbxRequired = wx.CheckBox( self, wx.ID_ANY, u"Not null", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.cbxRequired, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.cbxPrimary = wx.CheckBox( self, wx.ID_ANY, u"Primary", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.cbxPrimary, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.btnAdd = wx.Button( self, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer2.Add( self.btnAdd, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		
		bSizer7.Add( gSizer2, 1, wx.EXPAND, 5 )
		
		
		bSizer3.Add( bSizer7, 1, wx.EXPAND, 5 )
		
		bSizer8 = wx.BoxSizer( wx.VERTICAL )
		
		self.txtText = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.TE_MULTILINE )
		bSizer8.Add( self.txtText, 1, wx.ALL|wx.EXPAND, 5 )
		
		self.btnGenerate = wx.Button( self, wx.ID_ANY, u"Generar", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer8.Add( self.btnGenerate, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALL, 5 )
		
		
		bSizer3.Add( bSizer8, 2, wx.EXPAND, 5 )
		
		
		self.SetSizer( bSizer3 )
		self.Layout()

		self.btnAdd.Bind(wx.EVT_BUTTON,self.add_to_textbox)
		self.btnGenerate.Bind(wx.EVT_BUTTON,self.generate_code_table)

		self.table_name_check()
		self.db = db

	def table_name_check(self):
		while self.table_name == '':
			dlg = wx.TextEntryDialog(self, 'Table', 'Enter table name')
			dlg.ShowModal()
			self.table_name = dlg.GetValue()

		self.txtText.SetValue("create table "+self.table_name+'(\n')

	def add_to_textbox(self, evt):
		name = self.txtName.GetValue()
		value = self.spinLong.GetValue()
		typef = self.cboType.GetValue()
		required = self.cbxRequired.GetValue()
		primary = self.cbxPrimary.GetValue()
		default = self.txtDefault.GetValue()

		r,p,d = '', '',''
		if required == True:
			r = 'not null'
		if primary == True:
			p = 'auto_increment primary key'

		if default != '':
			if default.find('&i'):
				d = ' default '+default+ ' '
				d = d.replace('&i', '')
			elif default.find('&s'):
				d = " default '"+default+ "' "
				d = d.replace('&s', '')

		if typef=='date':
			self.txtText.AppendText(name +' '+ typef+ ' ' + r +' '+ p + d +',\n')
		else:
			self.txtText.AppendText(name +' '+ typef+'('+str(value)+') '+ r +' '+ p + d +',\n')

		self.txtName.SetValue('')
		self.spinLong.SetValue(1)
		self.cbxRequired.SetValue(False)
		self.cbxPrimary.SetValue(False)
		self.txtDefault.SetValue('')


	def generate_code_table(self, evt):
		text = self.txtText.GetValue()
		text = text[:-1]
		text = text[:-1]
		text = text + ')Engine=innodb;'
		self.controller.Database_Name= self.db
		self.controller.create_new_table(text)
		#print(text)

class new_row_dialog ( wx.Dialog ):
	controller = Controller()

	array_data = []
	array_value = []
	
	def __init__( self, parent, table, database):
		wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add row", pos = wx.DefaultPosition, size = wx.Size( 539,288 ), style = wx.DEFAULT_DIALOG_STYLE )
		
		self.table = table
		self.database = database

		self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )
		
		bSizer10 = wx.BoxSizer( wx.HORIZONTAL )
		
		gSizer3 = wx.GridSizer( 0, 2, 0, 0 )
		
		self.lblColumn = wx.StaticText( self, wx.ID_ANY, u"Column", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblColumn.Wrap( -1 )
		gSizer3.Add( self.lblColumn, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		cboColumnsChoices = self.coulmns_name()
		self.cboColumns = wx.ComboBox( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, cboColumnsChoices, 0 )
		gSizer3.Add( self.cboColumns, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		#

		self.lbl2 = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lbl2.Wrap( -1 )
		gSizer3.Add( self.lbl2, 0, wx.ALL, 5 )
		
		self.lblRequerido = wx.StaticText( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblRequerido.Wrap( -1 )
		self.lblRequerido.SetForegroundColour( wx.Colour( 255, 0, 0 ) )
		
		gSizer3.Add( self.lblRequerido, 0, wx.ALL, 5 )
		
		#
		self.lblColumn = wx.StaticText( self, wx.ID_ANY, u"Value", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.lblColumn.Wrap( -1 )
		gSizer3.Add( self.lblColumn, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		self.txtvalue = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size( 150,-1 ), 0 )
		gSizer3.Add( self.txtvalue, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		#
		
		self.btnAdd = wx.Button( self, wx.ID_ANY, u"Add", wx.DefaultPosition, wx.DefaultSize, 0 )
		gSizer3.Add( self.btnAdd, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		bSizer10.Add( gSizer3, 0, wx.EXPAND, 5 )
		
		bSizer12 = wx.BoxSizer( wx.VERTICAL )
		
		self.txtSQL = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.HSCROLL|wx.TE_MULTILINE|wx.TE_READONLY )
		bSizer12.Add( self.txtSQL, 1, wx.ALL|wx.EXPAND, 5 )
		
		bSizer10.Add( bSizer12, 1, wx.EXPAND, 5 )

		bSizer121 = wx.BoxSizer( wx.HORIZONTAL )
		
		self.btnSave = wx.Button( self, wx.ID_ANY, u"Save", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer121.Add( self.btnSave, 0, wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_LEFT|wx.ALL, 5 )
		
		self.btnClean = wx.Button( self, wx.ID_ANY, u"Clean", wx.DefaultPosition, wx.DefaultSize, 0 )
		bSizer121.Add( self.btnClean, 0, wx.ALIGN_CENTER_HORIZONTAL|wx.ALIGN_CENTER_VERTICAL|wx.ALL, 5 )
		
		
		bSizer12.Add( bSizer121, 0, wx.ALIGN_LEFT|wx.EXPAND, 5 )
		
		self.SetSizer( bSizer10 )
		self.Layout()
		
		self.Centre( wx.BOTH )

		#self.Bind(wx.EVT_COMBOBOX, self.add_to_textbox, self.cboColumns)
		self.btnAdd.Bind(wx.EVT_BUTTON,self.add_to_textbox)
		self.btnClean.Bind(wx.EVT_BUTTON,self.clean)
		self.btnSave.Bind(wx.EVT_BUTTON,self.save)
	
	def coulmns_name(self):
		self.rows = self.controller.get_header_table("SHOW COLUMNS FROM " + self.table, self.database)
		data = []		
		
		for row in self.rows:
			if  row[3] == 'PRI':
				if row[5] == 'auto_increment':
					print('No se necesita')
				else:
					data.append(row[0])
			else:
				data.append(row[0])

		return data

	def add_to_textbox(self, evt):
		self.rows = self.controller.get_header_table("SHOW COLUMNS FROM " + self.table, self.database)
		
		col = self.cboColumns.GetValue()
		val = self.txtvalue.GetValue()

		#print(data)
		self.txtSQL.AppendText(col + ':' + val + '\n')

		self.txtvalue.SetValue("")

	def clean(self, evt):
		self.txtSQL.SetValue("")

	def save(self, evt):
		print('-----------')
		data = self.txtSQL.GetValue()
		sql = 'insert into ' + self.table + '('
		#print(sql)
		col = ''
		val = ''
		rows = data.split('\n')
		n = len(rows)
		n = n -1 
		rows.pop(n)
		print(rows)

		for row in rows:
			p = str(row).split(':')
			col = col + str(p[0]) + ','
			val = val + str(p[1]) + ','

		sql = sql + col[:-1]
		sql[:-1]
		sql = sql + ') values("' + val[:-1] + '");'
		print(sql)
		self.controller.execute(self.database, sql)

app = wx.App()
main(None)
app.MainLoop()

