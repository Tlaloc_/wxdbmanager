# -*- coding: utf-8 -*- 

import json 

from DBManager import DBManager

class Controller(DBManager):
	"""docstring for Controller"""

	Host_Name = ""
	User_Name = ""
	Password = ""
	Database_Name = ""

	

	def __init__(self):
		pass

	def execute(self, db, query):
		self.Database_Name = db
		self.varable_connection()
		self.connection_start()
		m = self.execute_query(query)
		print(m)

	def delete_table_item(self, table, db, *data):
		self.Database_Name = db
		self.varable_connection()
		self.connection_start()
		m = self.seleccionar_dos("SHOW COLUMNS FROM "+table)
		data_column = []
		data_column_info = []
		i = 0
		for x in m:
			data_column.append(x[0])
			data_column_info.append(x[1])
			i += 1

		sql = 'DELETE FROM '+table+' WHERE '

		ite = 0
		for iterador in m:
			if str(data_column_info[ite]).count('int('):
				sql = sql + data_column[ite] + '=' + data[0][ite] + ' AND '
			else:
				sql = sql + data_column[ite] + '="' + data[0][ite] +'" AND '
			ite += 1

		sql = sql[:-4] + ';'

		r = self.execute_query(sql)
		print(r)

	def truncate_table(self, db, table):
		self.Database_Name = db
		self.varable_connection()
		self.connection_start()
		m = self.seleccionar_dos("TRUNCATE TABLE "+table)
		print(m)

	def get_databases(self):
		query="show databases"
		m = self.seleccionar_dos(query)
		#print("get_databases: "+ str(m))
		return m

	def get_data(self,table,db):
		self.Database_Name = db
		self.varable_connection()
		self.connection_start()
		m = self.seleccionar_dos("select * from "+table)
		#print(m)
		return m

	def get_header_table(self,sql,db):
		self.Database_Name = db
		self.varable_connection()
		self.connection_start()
		m = self.seleccionar_dos(sql)
		#print(m)
		return m

	def create_new_table(self,sql):
		print(self.Database_Name)
		self.varable_connection()
		self.connection_start()
		m = self.execute_query(sql)
		print(m)
		#return m

	def get_tables_from_database(self, table):
		self.Database_Name = table
		query="show tables"
		self.connection_start()
		m = self.seleccionar_dos(query)
		#print(m)
		return m

	def create_new_database(self,db):
		self.check_connection_fail()
		#print(self.Password)
		sql = "CREATE DATABASE "+db+" CHARACTER SET 'UTF8' COLLATE 'utf8_general_ci';"
		m = self.execute_query(sql)
		#print(m)
		return m

	def delete_database(self,db):
		sql = "DROP DATABASE "+db+";"
		m = self.execute_query(sql)
		self.check_connection_fail()
		self.connection_start()
		print('f = delete_database: '+ str(m))
		return m

	def delete_table(self,table):
		sql = "DROP table "+table+";"
		m = self.execute_query(sql)
		self.check_connection_fail()
		self.connection_start()
		print('f = delete_table: '+ str(m))
		print(m)

	def check_connection_fail(self):
		self.varable_connection()
		e = self.connection_start()
		print(e)
		return e

	def varable_connection(self):
		json_data=open('connection.json')
		data = json.load(json_data)
		self.Host_Name = data['conn'][0]
		self.User_Name = data['conn'][1]
		self.Password = data['conn'][2]

	def read_json(self):
		json_data=open('connection.json')
		data = json.load(json_data)
		return data['conn']

	def write_json_conn(self):

		data_list = []
		data_list.append(self.Host_Name)
		data_list.append(self.User_Name)
		data_list.append(self.Password)
		data_list.append(self.Database_Name)

		data_json = {}
		json_data = json.dumps(data_list)
		data_json['conn'] = json.loads(json_data)

		json_data = json.dumps(data_json)
		decoded = json.loads(json_data)

		with open('connection.json', 'w') as outfile:
			json.dump(decoded, outfile)

"""
c = Controller()
c.escribir()
"""