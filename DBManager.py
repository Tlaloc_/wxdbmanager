#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, MySQLdb


class DBManager():
	"""docstring for DBManager"""

	connection = None
	cursor = None

	def connection_start(self):
		try:
			self.connection = MySQLdb.Connection(host=self.Host_Name, user=self.User_Name, passwd=self.Password,db=self.Database_Name)
			self.cursor = self.connection.cursor()
		except MySQLdb.Error, e:
			return e

	def connection_close(self):
		self.connection.close()
		self.cursor.close()

	def execute_query(self, query):
		#print('f = execute_query '+query)
		try:
			self.connection = MySQLdb.Connection(host=self.Host_Name, user=self.User_Name, passwd=self.Password,db=self.Database_Name)
			self.cursor = self.connection.cursor()
			self.cursor.execute(query)
			m = self.connection.commit()
			self.connection_close()
			return m
		except MySQLdb.Error, e:
			return e

	def seleccionar_dos(self,query,dbt=''):
		#print(query)
		try:
			self.connection = MySQLdb.Connection(host=self.Host_Name, user=self.User_Name, passwd=self.Password,db=self.Database_Name)
			cursor = self.connection.cursor()
			self.cursor.execute(query)
			cursor.execute(query)
			rows = cursor.fetchall()
			self.connection.close()
			cursor.close()

			return rows
		except MySQLdb.Error, e:
			print(e)
			#print('..........')
			return e